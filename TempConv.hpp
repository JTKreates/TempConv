#include <iostream>

class TempConv
{
public:
    TempConv();

    void ctof(double v);
    void ctok(double v);
    void ftoc(double v);
    void ftok(double v);
    void ktoc(double v);
    void ktof(double v);
};
