#include "TempConv.hpp"

TempConv::TempConv() {}

void TempConv::ctof(double v)
{
    double factor;
    factor = 212 - 32;

    double fah;
    fah = factor * v / 100 + 32;

    std::cout << "F value is : " << fah << std::endl;
}

void TempConv::ctok(double v)
{
    double kel;
    kel = v + 273.15;

    std::cout << "K value is : " << kel << std::endl;
}

void TempConv::ftoc(double v)
{
    double factor;
    factor = 212 - 32;

    double cel;
    cel = ((v - 32) * 100) / factor;

    std::cout << "C value is : " << cel << std::endl;
}

void TempConv::ftok(double v)
{
    double factor;
    factor = 212 - 32;

    double kel;
    kel = (((v - 32) * 100) / factor) + 273.15;

    std::cout << "K value is : " << kel << std::endl;
}

void TempConv::ktoc(double v)
{
    double cel;
    cel = v - 273.15;

    std::cout << "C value is : " << cel << std::endl;
}

void TempConv::ktof(double v)
{
    double factor;
    factor = 212 - 32;

    double fah;
    fah = (((v - 273.15) * factor) / 100) - 32;

    std::cout << "F value is : " << fah << std::endl;
}