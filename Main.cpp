#include "TempConv.hpp"

int main()
{
    double temp;

    TempConv tc;

    int choice = 0;
    while(choice == 0, 1, 2, 3, 4, 5, 6, 7)
    {
        std::cout << "Press 1 for Fahrenheit to Celsius" << std::endl;
        std::cout << "Press 2 for Celsius to Fahrenheit" << std::endl;
        std::cout << "Press 3 for Fahrenheit to Kelvin" << std::endl;
        std::cout << "Press 4 for Kelvin to Fahrenheit" << std::endl;
        std::cout << "Press 5 for Celsius to Kelvin" << std::endl;
        std::cout << "Press 6 for Kelvin to Celsius" << std::endl;
        std::cout << "Press 7 to quit the application" << std::endl;
        std::cin >> choice;

        switch(choice)
        {
            case 1:
                std::cout << "Enter the temp you want to convert" << std::endl;
                std::cin >> temp;
                tc.ftoc(temp);
                break;
            case 2:
                std::cout << "Enter the temp you want to convert" << std::endl;
                std::cin >> temp;
                tc.ctof(temp);
                break;
            case 3:
                std::cout << "Enter the temp you want to convert" << std::endl;
                std::cin >> temp;
                tc.ftok(temp);
                break;
            case 4:
                std::cout << "Enter the temp you want to convert" << std::endl;
                std::cin >> temp;
                tc.ktof(temp);
                break;
            case 5:
                std::cout << "Enter the temp you want to convert" << std::endl;
                std::cin >> temp;
                tc.ctok(temp);
                break;
            case 6:
                std::cout << "Enter the temp you want to convert" << std::endl;
                std::cin >> temp;
                tc.ktoc(temp);
                break;
            case 7:
                return 0;
            default:
                return -1;
        }

        std::cin.clear();
    }

    return -1;
}